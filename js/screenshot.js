const { desktopCapturer } = require('electron');

/**
 * Create a screenshot of the entire screen using the desktopCapturer module of Electron.
 *
 * @param callback {Function} callback receives as first parameter the base64 string of the image
 * @param imageFormat {String} Format of the image to generate ('image/jpeg' or 'image/png')
 **/
function capture(callback, imageFormat) {
	var _this = this;
	this.callback = callback;
	imageFormat = imageFormat || 'image/jpg';
	var ratio = 0.7;

	this.handleStream = (stream) => {
		// Create hidden video tag
		var video = document.createElement('video');
		video.style.cssText = 'position:absolute;top:-10000px;left:-10000px;';

		// Event connected to stream
		video.onloadedmetadata = function() {
			// Set video ORIGINAL height (screenshot)
			video.style.height = this.videoHeight + 'px'; // videoHeight
			video.style.width = this.videoWidth + 'px'; // videoWidth

			// Create canvas
			var canvas = document.createElement('canvas');
			canvas.width = this.videoWidth * ratio;
			canvas.height = this.videoHeight * ratio;
			var ctx = canvas.getContext('2d');

			var playPromise = video.play();

			if (playPromise !== undefined) {
				playPromise
					.then((_) => {
						// Automatic playback started!
						// Show playing UI.
						// We can now safely pause video...
						video.pause();
					})
					.catch((error) => {
						// Auto-play was prevented
						// Show paused UI.
					});
			}

			// Draw video on canvas
			ctx.drawImage(video, 0, 0, canvas.width, canvas.height);

			if (_this.callback) {
				// Save screenshot to base64
				_this.callback(canvas.toDataURL(imageFormat, 1));
			} else {
				console.log('Need callback!');
			}

			try {
				// Destroy connect to stream
				stream.getTracks()[0].stop();
			} catch (e) {}
		};

		video.srcObject = stream;
		document.body.appendChild(video);

		// Remove hidden video tag
		video.remove();
	};

	this.handleError = function(e) {
		console.log(e);
	};

	desktopCapturer.getSources({ types: ['screen'] }).then(async sources => {
		for (const source of sources) {
			try {
				const stream = await navigator.mediaDevices.getUserMedia({
					audio: false,
					video: {
						mandatory: {
							chromeMediaSource: 'desktop',
							chromeMediaSourceId: source.id,
							minWidth: 1280,
							maxWidth: 1280,
							minHeight: 720,
							maxHeight: 720
						}
					}
				})
				handleStream(stream)
				} catch (e) {
				handleError(e)
			}
		}
	});
}
