const log = require('electron-log');
const isOnline = require('is-online');

var needle = require('needle');
needle.defaults({
	open_timeout: 120000
});

var apiService = function(endpoint, parameters, callback, forwardData = null) {
	isOnline().then((online) => {
		if (online) {
			var targetURI = forwardData.host + '/api' + endpoint;
			log.info('Processing apiService using ' + endpoint);

			var options = {
				headers: { 
					accept: 'application/json', 
					authorization: 'Bearer ' + forwardData.access_token,
					connection: 'keep-alive'
				}
			};

			needle.post(targetURI, parameters, options, function(err, resp, body) {
				serviceReady = true;

				if (err) {
					log.error(err);
					log.error('Uh-oh, apiService error! ' + err + ' using ' + endpoint);
					forwardData.statusMessage = err;
				} else {
					if(resp) {
						log.info('Remaining Request Limit: ' + resp.headers['x-ratelimit-remaining']);
						
						forwardData.statusMessage = resp.statusMessage;

						if(resp.statusMessage == 'OK') {
							forwardData.mainWindow.webContents.send('hide-element', { element: '.connecting-error' });
						} else {
							forwardData.retry_after = resp.headers['retry-after'];
						}
					} else {
						forwardData.statusMessage = 'ECONNRESET';
					}
				}

				if(callback) {
					callback(body, forwardData);
				}
			});
		} else {
			forwardData.statusMessage = 'No Internet Connection';

			if(callback) {
				callback(null, forwardData);
			}
		}
	});
}

module.exports = apiService;