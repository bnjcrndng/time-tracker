$(document).on('click', '#lgn-btn', function(e) {
	e.preventDefault();

	var noError = true;
	var errorMsg = '';
	var postData = {};

	$('#login-form input').each(function(i, input) {
		errorMsg = validateField($(input), postData);
		if (errorMsg != '') noError = false;
	});

	if (noError) {
		postData['password'] = $.sha1(postData['password']);
		main.login(postData);
	}

	return noError;
});

$(document).on('keyup', function(event) {
	if (event.key === 'Enter') {
		event.preventDefault();
		$('#lgn-btn').trigger('click');
	}
});

function validateField(infield, postData, isselect) {
	var fieldVal = infield.val();
	if (fieldVal != null && typeof fieldVal === 'string') {
		fieldVal = $.trim(fieldVal);
	}

	var fieldName = infield.attr('name');
	var validate = infield.attr('data-validate');
	//var efldName = fieldName.replace("_", " ");

	if (infield.is(':checkbox') || infield.is(':radio')) {
		if (infield.prop('checked')) {
			if (infield.is(':checkbox') && postData[fieldName] != null && postData[fieldName] != '') {
				postData[fieldName] += ',' + fieldVal;
			} else {
				postData[fieldName] = fieldVal;
			}
		} else {
			var uncheckVal = infield.attr('data-uncheckval');
			if (uncheckVal) {
				postData[fieldName] = uncheckVal;
			}
		}

		return '';
	}

	var errorMsg = '';

	infield.removeClass('error_field');
	$('#' + fieldName + '_emsg').remove();
	if (isselect) infield.parent('.select-wrapper').removeClass('error_field');

	postData[fieldName] = fieldVal;
	var minVal = infield.attr('data-minimum');
	var maxVal = infield.attr('data-maximum');
	if (infield.hasClass('required')) {
		if ((maxVal != null || minVal != null) && fieldVal != null) {
			if (fieldVal == '') {
				errorMsg = 'This field is required.\n';
			} else {
				errorMsg = validateMinMaxValue(fieldVal, minVal, maxVal, validate);
			}
		} else if (fieldVal == null || fieldVal == '' || (isselect && fieldVal == '0')) {
			errorMsg = 'This field is required.\n';
		} else if ((fieldName == 'email' || validate == 'email') && !isEmail(fieldVal)) {
			errorMsg = 'Invalid email address.\n';
		} else if (validate && validate == 'date' && !isValidDate(fieldVal)) {
			errorMsg = 'Invalid date value or format.\n';
		} else if (validate && validate == 'string') {
			var strLength = infield.attr('data-length');
			if (strLength != '' && strLength > 0 && fieldVal.length != strLength) {
				errorMsg = 'Invalid ticket number.\n';
			}
		}
	} else {
		if (minVal != null || maxVal != null) {
			errorMsg = validateMinMaxValue(fieldVal, minVal, maxVal, validate);
		} else if (validate && fieldVal != '' && validate == 'email' && !isEmail(fieldVal)) {
			errorMsg = 'Invalid email address.\n';
		} else if (validate && validate == 'date' && fieldVal != '' && !isValidDate(fieldVal)) {
			errorMsg = 'Invalid date value or format.\n';
		}
	}

	if (errorMsg == '' && validate != '') {
		if (!isValidNumberValue(fieldVal, validate)) {
			errorMsg = 'Invalid number value.\n';
		}
	}

	infield.attr('title', errorMsg);
	if (errorMsg != '') {
		addFieldErrorMsg(infield, fieldName, errorMsg, isselect);
	}

	return errorMsg;
}

function isValidNumberValue(fieldVal, validate) {
	if (validate == 'number_only') {
		if (!/^[0-9,.]*$/.test(fieldVal) || countSimilarCharacters(fieldVal, '\\.') > 1) {
			return false;
		}
	} else if (validate == 'number_range' || validate == 'contact_number') {
		if (!/^[0-9,.\-]*$/.test(fieldVal) || countSimilarCharacters(fieldVal, '\\.') > 1) {
			return false;
		}
	}

	return true;
}

function validateMinMaxValue(fieldVal, minVal, maxVal, validate) {
	var errorMsg = '';

	if (!isValidNumberValue(fieldVal, validate)) {
		return 'Invalid number value.\n';
	}

	if (
		(minVal != null && parseInt(fieldVal) < parseInt(minVal)) ||
		(maxVal != null && parseInt(fieldVal) > parseInt(maxVal))
	) {
		if (maxVal != null && parseInt(fieldVal) > parseInt(maxVal)) {
			errorMsg = 'Value must be less than or equal to ' + maxVal + '\n';
		} else if (minVal != null && parseInt(fieldVal) < parseInt(minVal)) {
			errorMsg = 'Value must be at least ' + minVal + '\n';
		}
	}

	return errorMsg;
}

function addFieldErrorMsg(infield, fieldName, errorMsg, isselect) {
	infield.addClass('error_field');
	if (infield.hasClass('schedule-time')) {
		infield.parent().find('div.error-msg').remove();
	}

	infield.parent().append('<div id="' + fieldName + '_emsg" class="error-msg">' + errorMsg + '</div>');

	if (isselect) {
		infield.parent('.select-wrapper').addClass('error_field');
	}
}

function clearValidationError(infield, isselect) {
	var fieldName = infield.attr('name');

	infield.removeClass('error_field');
	$('#' + fieldName + '_emsg').empty();
	if (isselect) infield.parent('.select-wrapper').removeClass('error_field');
}

function capitalizeWord(str) {
	str = str.toLowerCase().replace(/\b[a-z]/g, function(letter) {
		return letter.toUpperCase();
	});

	return str;
}

function isEmail(email) {
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,6})+$/;
	return regex.test(email);
}