const remote = require('electron').remote;
const main = remote.require('./main.js');
const ipcRenderer = require('electron').ipcRenderer;
const shell = require('electron').shell;
const isOnline = require('is-online');
const ioHook = require('iohook');
var moment = require('moment');
var mouseCounter = 0;
var keyboardCounter = 0;
var checkOnlineStatusTimer = 120000; //check online status 2/m
var sendUserActivityTrackerTimer = 1800000; //send user activity every 30 minutes
var trackerSettings;
var onlineStatusIsActive = false;
var screenshotTimeOut;
var userActivityTrackerTimeOut;
var screenshotMin = 8;
var screenshotMax = 15;
var isInternetCheckActive;
var isRetryCheckActive;

ipcRenderer.on('update-bar', function(event, data) {
	$('.update-percentage').text(data.percentage);
	$('.progress-bar').css('width', data.percentage);
});

ipcRenderer.on('show-element', function(event, data) {
	$(data.element).show();
});

ipcRenderer.on('hide-element', function(event, data) {
	$(data.element).hide();
});

ipcRenderer.on('check-internet', function(event, data) {
	if(data.isOnline) {
		$('.connecting-error').hide();
		$('.connection-back').show();

		setTimeout(function () {
			$('.connection-back').hide();
		}, 7000);

		$('#lgn-btn').prop('disabled', false);
		clearInterval(isInternetCheckActive);
		isInternetCheckActive = null;
	} else {
		$('.connecting-error').show();
		$('.connection-back').hide();

		if(isInternetCheckActive == null) {
			internetCheckerCounter();
		}

		$('#lgn-btn').prop('disabled', true);
	}
});

ipcRenderer.on('add-active-class', function(event, data) {
	$('.tasks-list li').removeClass('active');
	$(data.element).addClass('active');
	$('#preloader-background').hide();
});

ipcRenderer.on('remove-active-class', function(event, data) {
	$(data.element).removeClass('active');
});

ipcRenderer.on('login-error', function(event, data) {
	var ajax = $.get('./login.html', function(html) {
		$('#app').html(html);
	})
	
	ajax.always(function(res) {
		var errorMessage = '<div class="error-msg">The email and password you entered didn\'t match. </div>';
		
		if(data.code == 903) {
			errorMessage = '<div class="error-msg">Password expired. Go to <a class="link-primary" href="https://mydiversify.com">MyDiversify</a> to update your password.</div>';
		}

		$('.log-error').empty().html(errorMessage);
		$('#email').val(data.email);
		$('#password').val('');

		$('#preloader-background').hide();
	});
});

ipcRenderer.on('append-login-page', function() {
	var ajax = $.get('./login.html', function(data) {
		$('#app').empty();
		$('#app').html(data);
	})
	
	ajax.always(function() {
		isOnline().then((online) => {
			if (!online) {
				$('#lgn-btn').prop('disabled', true);
				$('.connecting-error').show();
				main.openErrorRibbon();
			} else {
				main.checkForUpdates(false);
			}
		});

		$('#preloader-background').hide();
	});
});

ipcRenderer.on('append-stats-page', function (event, data) {
	var ajax = $.get('./stats.html', function(html) {
		$('#app').empty();
		$('#app').html(html);
	});

	ajax.always(function() {
		if(data.setTimeTrackerData == true) {
			setTimeTrackerData(data);
		}
	});
});

ipcRenderer.on('append-action', function(event, data) {
	var retrySecondsCounter = data.retrySeconds / 1000;
	var retryCounterHTML = '';
	
	if(data.showRetrySeconds) {
		retryCounterHTML = '<span class="retry-counter loading-dots">'+ retrySecondsCounter +'s</span>';
		retryCounterDecrease(retrySecondsCounter);
	}

	$('#action-in-progress').empty().append(data.action + retryCounterHTML);
});

ipcRenderer.on('remove-action', function(event, data) {
	clearInterval(isRetryCheckActive);
	$('#action-in-progress').empty();
});

$(document).on('click', '#time-in, #time-in-wfh', function() {
	main.timeIn(remote.getGlobal('tracker_id'), remote.getGlobal('user_id'), $(this).data('is-onsite'));
});

$(document).on('click', '#time-out', function() {
	main.timeOut(remote.getGlobal('tracker_id'), remote.getGlobal('user_id'));
});

$(document).on('click', '#logout', function() {
	main.logout();
});

$(document).on('click', '#sync', function() {
	main.openStats('refresh');
});

$(document).on('click', '.tasks-list li:not(".disabled"):not(".active")', function() {
	main.task($(this).data('id'), remote.getGlobal('tracker_id'), remote.getGlobal('user_id'));
});

$(document).on('click', 'a[href^="http"]', function(e) {
	e.preventDefault();
    shell.openExternal(this.href);
});

function startTime() {
    $('#live-time').html(moment().format('hh:mm:ss A'));
    setTimeout(startTime, 1000);
}

function setTimeTrackerData(data) {
	startTime();

	$('#full-name').text(data.full_name);

	trackerSettings = data.trackerSettings;

	if(data.message  && data.message != null) {
		$('#message').html(data.message + ' <a href="https://mydiversify.com" class="blocker-text"> Go to MyDiversify <i class="fa fa-chevron-right small"></i></a>').addClass('d-block');
		$('.stats-bg').css('background', 'linear-gradient(360deg, rgba(118,91,167,1) 50%,#1793b8a6 75%');
	}
	
	if (data.enabledTimeIn == 1) {
		$('#time-in, #time-in-wfh').prop('disabled', false);
	} else {
		$('#time-in, #time-in-wfh').prop('disabled', true);
	}
	
	if (data.enabledTimeOut == 1) {
		$('#time-out').prop('disabled', false);
	} else {
		$('#time-out').prop('disabled', true);
	}

	if( (trackerSettings && trackerSettings.requireActionAMR > 0) && !($('#time-in').prop('disabled') && $('#time-out').prop('disabled')) ) {
		$('.require-action').show();
	}

	if(data.enabledTimeIn == 1 || data.enabledTimeOut == 1) {
		if(isEnableInternetMonitoring()) {
			if(data.refresh == 1) {
				if (!onlineStatusIsActive) {
					onlineStatusIsActive = true;
					setTimeout(function() {onlineStatusPerMinute()}, checkOnlineStatusTimer);
				}
			} else {
				onlineStatusIsActive = true;
				setTimeout(function() {onlineStatusPerMinute()}, checkOnlineStatusTimer);
			}
		} else {
			onlineStatusIsActive = false;
		}
	}

	if(isEnableScreenshotCapture()) {
		if(trackerSettings.screenshot_interval_from > 0) screenshotMin = parseInt(trackerSettings.screenshot_interval_from);
		if(trackerSettings.screenshot_interval_to > 0) screenshotMax = parseInt(trackerSettings.screenshot_interval_to);

		if(data.refresh == 1) {
			if (screenshotTimeOut == null) {
				screenshotTimeOut = 'temp';
				setTimeout(function() { startCapturing(); }, 90000); // start the screenshot 90 seconds
			}
		} else {			
			screenshotTimeOut = 'temp';
			setTimeout(function() { startCapturing(); }, 90000); // start the screenshot 90 seconds
		}
	} else {
		clearTimeout(screenshotTimeOut);
		screenshotTimeOut = null;
	}
	
	if ($('#time-in').prop('disabled') && !$('#time-out').prop('disabled')) {
		$('.tasks-list li').removeClass('disabled');
		
		if(isEnableUserActivityTracking()) {
			if(data.refresh == 1) {
				if (userActivityTrackerTimeOut == null) {
					ioHookStart();
					setTimeout(function () { sendUserActivityCounter(); }, sendUserActivityTrackerTimer);
					userActivityTrackerTimeOut = 'temp';
				}
			} else {
				ioHookStart();
				setTimeout(function () { sendUserActivityCounter(); }, sendUserActivityTrackerTimer);
				userActivityTrackerTimeOut = 'temp';
			}
		} else {
			clearTimeout(userActivityTrackerTimeOut);
			userActivityTrackerTimeOut = null;

			ioHook.stop();
		}
	} else if($('#time-in').prop('disabled') && $('#time-out').prop('disabled')) {
		clearTimeout(userActivityTrackerTimeOut);
		userActivityTrackerTimeOut = null;

		onlineStatusIsActive = false;
		ioHook.stop();
	}
	
	if (!$('#time-in').prop('disabled')) {
		$('.tasks-list li').removeClass('active');
	} else {
		$('.tasks-list li[data-task="' + data.taskName + '"]').addClass('active');
	}

	if(data.status == 929) {
		$('#time-in-wfh').prop('disabled', false);
		$('#time-in').prop('disabled', true);
	}
	
	$('.stats').addClass('animate');
}

function startCapturing() {
	var dateCaptured = new Date();
	console.log('Screenshot captured at: ' + dateCaptured);

	capture(function(imageData) {
		main.screenShotURL(imageData, remote.getGlobal('tracker_id'), remote.getGlobal('user_id'));
	}, 'image/jpg');

	var rand = Math.floor(Math.random() * (screenshotMax - screenshotMin + 1) + screenshotMin);
	console.log('Next screenshot after ' + rand + ' minutes');
	
	screenshotTimeOut = setTimeout(startCapturing, rand * 60000);
}

ipcRenderer.on('online-status-per-minute', function(event, data) {
	if(onlineStatusIsActive) {
		setTimeout(onlineStatusPerMinute, checkOnlineStatusTimer);
	}
});

function onlineStatusPerMinute() {
	main.onlineStatus(remote.getGlobal('user_id'));
}

function sendUserActivityCounter() {
	main.sendUserActivityTracker(
		remote.getGlobal('tracker_id'), 
		remote.getGlobal('user_id'),
		keyboardCounter,
		mouseCounter
	);

	mouseCounter = 0;
	keyboardCounter = 0;

	userActivityTrackerTimeOut = setTimeout(sendUserActivityCounter, sendUserActivityTrackerTimer);
}

function ioHookStart() {
	ioHook.start();

	ioHook.on("mouseclick", event => {
		mouseCounter++;
	});
	
	ioHook.on("keyup", event => {
		keyboardCounter++;
	});
}

function isEnableInternetMonitoring(){
	if(trackerSettings.disable_time_tracker_internet_monitoring == null || trackerSettings.disable_time_tracker_internet_monitoring === undefined || trackerSettings.disable_time_tracker_internet_monitoring == 0) {
		return true;
	}else{
		return false;
	}
}

function isEnableScreenshotCapture(){
	if(trackerSettings.disable_time_tracker_screenshots == null || trackerSettings.disable_time_tracker_screenshots === undefined || trackerSettings.disable_time_tracker_screenshots == 0) {
		return true;
	}else{
		return false;
	}
}

function isEnableUserActivityTracking(){
	if(trackerSettings.enable_user_activity_tracking == null || trackerSettings.enable_user_activity_tracking === undefined || trackerSettings.enable_user_activity_tracking == 0) {
		return false;
	}else{
		return true;
	}
}

function retryCounterDecrease(retrySecondsRemaining) {
    var i = retrySecondsRemaining;
    var retrySecondsRemainingDecrease = function(){
        if (i != 0) {
			$('.retry-counter').text(i-- + 's');
        } 
    };
    isRetryCheckActive = setInterval(retrySecondsRemainingDecrease, 1000);
}

function internetCheckerCounter() {
	var i = 9;
    var internetCheckDecrease = function(){
        if (i < 0) i = 10;
		else $('.check-internet-timer').text(i-- + 's');
    };
	isInternetCheckActive = setInterval(internetCheckDecrease, 1000);
}