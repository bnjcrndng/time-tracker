'use strict';

const { app, BrowserWindow, net, dialog, Tray, Menu } = require('electron');
const { autoUpdater } = require('electron-updater');
const gotTheLock = app.requestSingleInstanceLock();
const isOnline = require('is-online');
const path = require('path');
const formatUrl = require('url').format;
const dotenv = require('dotenv');
dotenv.config();

var apiService = require('./js/api-service');
var needle = require('needle');
needle.defaults({
	open_timeout: 120000
});

let isSilent;
let updateDownloaded = false;
autoUpdater.autoDownload = true;

// node module for logging and file logging
const log = require('electron-log');
log.transports.console.format = '[{h}:{i}:{s}] > {text}';
log.transports.file.format = '[{y}-{m}-{d} {h}:{i}:{s}][{level}] {text}';
log.transports.file.resolvePath = () => path.dirname(app.getPath('exe')) + "/log.log";

// global reference to mainWindow (necessary to prevent window from being garbage collected)
let mainWindow;
let isQuiting;

var appVersion = app.getVersion();
var __this = this;

// Authentication Variables
var email;
var password;
var token_type;
var access_token;
var client_secret;
var client_id;
var host;

// Request Queue
var screenshotQueueArr = [];
var activityQueueArr = [];

//Global Variables
global.screenshotReady = true;
global.user_id = null;
global.tracker_id = null;

var contextMenu;
var retryTimeoutDefault = 20000; //default to 20 seconds

client_secret = 'uNg4BiFO0hWD4YpxTdd6Pq7xoGhfyDM7VQkSYBqT';
client_id = 5;
host = 'https://mydiversify.com';

var appEnv = 'production';

if(appEnv == 'local') {
	host = 'http://127.0.0.1:8000';
}

function createMainWindow() {
	const window = new BrowserWindow({
		title: 'MyDiversify App ' + appVersion,
		width: 350,
		height: 567,
		minWidth: 350,
		minHeight: 567,
		resizable: false,
		minimizable: true,
		maximizable: true,
		frame: true,
		backgroundColor: '#fff',
		vibrancy: 'popover',
		show: true,
		webPreferences: {
			nodeIntegration: true
		}
	});

	if (appEnv == 'local') window.webContents.openDevTools();

	window.setMenu(null);

	window.loadURL(
		formatUrl({
			pathname: path.join(__dirname, 'index.html'),
			protocol: 'file',
			slashes: true
		})
	);

	var tray = new Tray(path.join(__dirname, '/images/icon_x32.png'));

	contextMenu = Menu.buildFromTemplate([
		{
			label: 'Show',
			click: function() {
				window.show();
			}
		},
		{
			label: 'Quit',
			click: function() {
				app.quit();
				app.isQuiting = true;
			}
		}
	]);

	tray.setContextMenu(contextMenu);

	tray.setToolTip('Diversify Time Tracker');

	tray.on('double-click', () => {
		window.show();
	});

	window.on('show', function() {
		tray.focus();
	});

	window.on('closed', (e) => {
		mainWindow = null;
	});

	window.on('close', function(event) {
		if (!isQuiting) {
			event.preventDefault();
			window.hide();
			event.returnValue = false;
		}
	});

	window.webContents.on('devtools-opened', () => {
		window.focus();
		setImmediate(() => {
			window.focus();
		});
	});

	window.webContents.once('dom-ready', () => {
		window.webContents.send('append-login-page');
	});

	return window;
}

app.on('before-quit', function() {
	isQuiting = true;
});

// quit application when all windows are closed
app.on('window-all-closed', () => {
	// on macOS it is common for applications to stay open until the user explicitly quits
	if (process.platform !== 'darwin') {
		app.quit();
	}
});

app.on('activate', () => {
	// on macOS it is common to re-create a window even after all windows have been closed
	if (mainWindow === null) {
		mainWindow = createMainWindow();
	}
});

// create main BrowserWindow when electron is ready
app.on('ready', () => {
	mainWindow = createMainWindow();
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
exports.login = (postData) => {
	log.info('exports.login starts!');

	isOnline().then((online) => {
		if (online) {
			var options = {
				grant_type: 'password',
				client_id: client_id,
				client_secret: client_secret,
				email: postData.email,
				password: postData.password,
				scope: '*'
			};

			__this.showPreloader('Authenticating user credentials');
			
			needle.post(host + '/api/login', options, function(err, resp) {
				if (err) { //Retry Here
					log.error('Uh-oh, Login Error! ' + err);

					var retryAfter = __this.getRetrySecondsRemaining();

					__this.showPreloader('<p>Authenticating user credentials</p>' + err + '. Retrying after ', retryAfter, true);
					
					setTimeout(function () {
						__this.login(postData);
					}, retryAfter);
				} else {
					token_type = resp.body.token_type;
					access_token = resp.body.access_token;
					email = postData.email;
					password = postData.password;

					if (access_token) {
						log.info('Logged in successfully! ');

						if(mainWindow && mainWindow.webContents) {
							mainWindow.webContents.send('append-stats-page', {
								setTimeTrackerData: false
							});
						}

						__this.openStats('openStats');
					} else {
						if(resp && resp.body.status == 904) {
							log.warn('Uh-oh, Invalid Credentials!');

							if(mainWindow && mainWindow.webContents) mainWindow.webContents.send('login-error', {code: 904, email: email});
						} else if(resp && resp.body.status == 903) {
							log.warn('Uh-oh, Password Expired!');

							if(mainWindow && mainWindow.webContents) mainWindow.webContents.send('login-error', {code: 903, email: email});
						} else {
							log.error('Uh-oh, Login Error! ' + resp.statusMessage);

							var retryAfter = __this.getRetrySecondsRemaining(resp.headers['retry-after']);

							__this.showPreloader('<p>Authenticating user credentials</p>' + resp.statusMessage + '. Retrying after ', retryAfter, true);

							setTimeout(function () {
								__this.login(postData);
							}, retryAfter);
						}
					}
				}
			});
		} else {
			log.error('Uh-oh, Login Error! ' + 'No Internet Connection.');

			setTimeout(function() {
				if(mainWindow && mainWindow.webContents) mainWindow.webContents.send('append-login-page');
			}, 3000);
		}
	});
};

exports.openStats = (checkMode) => {
	log.info('exports.openStats starts!');

	__this.showPreloader('Retrieving user data');

	var forwardData = __this.getForwardData();
	forwardData.mode = checkMode;

	apiService(
		'/my-time-tracker-today',
		null,
		__this.openStatsCallback,
		forwardData
	);
};

exports.timeIn = (tracker_id, user_id, is_onsite) => {
	log.info('exports.timeIn starts!');

	__this.showPreloader('Saving attendance time-in');

	var forwardData = __this.getForwardData();
	forwardData.time_in = true;
	forwardData.time_out = false;
	forwardData.is_onsite = is_onsite;

	var parameters = {
		isonsite: is_onsite,
		browser: 'My Diversify Time Tracker',
		full_version: appVersion,
		major_version: 'v1',
		navigator_appName: 'Google Chrome ' + process.versions.chrome,
		navigator_userAgent: 'Electron',
		os: 'Electron ' + process.versions.electron
	}

	apiService(
		'/my-time-tracker/' + user_id + '/time-in/' + tracker_id,
		parameters,
		__this.timeInTimeOutCallback,
		forwardData
	);
};

exports.timeOut = (tracker_id, user_id) => {
	log.info('exports.timeOut starts!');

	__this.showPreloader('Saving attendance time-out');

	var forwardData = __this.getForwardData();
	forwardData.time_in = false;
	forwardData.time_out = true;

	apiService(
		'/my-time-tracker/' + user_id + '/time-out/' + tracker_id,
		null,
		__this.timeInTimeOutCallback,
		forwardData
	);
};

exports.task = (task_id, tracker_id, user_id) => {
	log.info('exports.task starts!');

	__this.showPreloader('Updating current task');

	var data = {};

	var forwardData = __this.getForwardData();
	forwardData.task_id = task_id;
	
	apiService(
		'/my-time-tracker/' + user_id + '/task/' + tracker_id + '/' + task_id,
		data,
		__this.taskCallback,
		forwardData
	);
};

exports.screenShotURL = (imageData, tracker_id, user_id) => {
	log.info('exports.screenShotURL starts!');

	if (screenshotReady) {
		screenshotReady = false;

		var data = {
			base64img: imageData
		};

		var forwardData = __this.getForwardData();
		forwardData.imageData = imageData;

		apiService(
			'/my-time-tracker/' + user_id + '/screenshot/' + tracker_id,
			data,
			__this.screenShotURLCallback,
			forwardData
		);
	} else {
		screenshotQueueArr.push({ image: imageData, tracker: tracker_id, user: user_id });
	}
};

exports.onlineStatus = (user_id) => {
	log.info('exports.onlineStatus starts!');

	var data = {};

	var forwardData = __this.getForwardData();

	apiService(
		'/my-online-status/' + user_id,
		data,
		__this.onlineStatusCallback,
		forwardData
	);
};

exports.sendUserActivityTracker = (tracker_id, user_id, keyboard_clicks, mouse_clicks) => {
	log.info('exports.sendUserActivityTracker starts!');

	var data = {
		keyboard_clicks: keyboard_clicks,
		mouse_clicks: mouse_clicks,
		tracker_id: tracker_id,
		user_id: user_id
	};

	var forwardData = __this.getForwardData();
	forwardData.keyboard_clicks = keyboard_clicks;
	forwardData.mouse_clicks = mouse_clicks;

    apiService(
	   	'/user-activity-tracker/' + user_id + '/' + tracker_id,
 	   	data,
 	  	__this.activityTrackerCallback,
		forwardData
    );
};

exports.logout = () => {
	log.info('exports.logout starts!');

	__this.showPreloader('Logging out');

	var data = {};
	var forwardData = __this.getForwardData();

	apiService(
		'/logout',
		data,
		null,
		forwardData
	);

	__this.resetVariable();

	mainWindow.setMenu(null);
	mainWindow.loadURL(
		formatUrl({
			pathname: path.join(__dirname, 'index.html'),
			protocol: 'file',
			slashes: true
		})
	);
	mainWindow.webContents.once('dom-ready', () => {
		mainWindow.webContents.send('append-login-page');
	});
};

exports.resetVariable = () => {
	email = null;
	password = null;
	token_type = null;
	access_token = null;
	screenshotQueueArr = [];
	activityQueueArr = [];
	user_id = null;
	tracker_id = null;
}

// Callbacks
exports.openStatsCallback = (body, forwardData) => {
	var enabledTimeIn = 0,
	enabledTimeOut = 0,
	tempMessage, 
	full_name = 'Unknown',
	refresh = 0,
	trackerSettings,
	data,
	tempMessage = '';

	if(body && body.data) {
		log.info('Successfully opened the stat pages');
		data = body.data;
		trackerSettings = data.trackerSettings;
		
		if(data.userData) {
			full_name = data.userData.first_name + ' ' + data.userData.last_name;
		}

		if (body.message == 'OK' && body.status == 1) {
			if (body.data.ttr_time_in == null) {
				enabledTimeIn = 1;
			} else if (body.data.ttr_time_out == null) {
				enabledTimeOut = 1;
			}
		} else {
			tempMessage = body.message;
		}

		if (forwardData && forwardData.mode == 'refresh') {
			refresh = 1;
		}

		user_id = data.ttr_user_id;
		tracker_id = data.ttr_id;

		if(mainWindow && mainWindow.webContents) {
			mainWindow.webContents.send('append-stats-page', {
				trackerSettings: trackerSettings,
				full_name: full_name,
				message: tempMessage,
				refresh: refresh,
				enabledTimeIn: enabledTimeIn,
				enabledTimeOut: enabledTimeOut,
				taskName: trackerSettings != null ? trackerSettings.task : '',
				setTimeTrackerData: true,
				status: body.status,
			});
		}

		__this.hidePreloader();
	} else { // Retry request
		log.error('Uh-oh, openStatsCallback error! ' + forwardData.statusMessage);

		var retryAfter = __this.getRetrySecondsRemaining(forwardData.retry_after);

		__this.showPreloader('<p>Retrieving user data.</p>' + forwardData.statusMessage + '. Retrying after ', retryAfter, true);
		
		setTimeout(function () {
			__this.openStats('openStats');
		}, retryAfter);
	}
}

exports.taskCallback = (body, forwardData) => {
	__this.hidePreloader();

	var retryAfter = __this.getRetrySecondsRemaining(forwardData.retry_after);

	if(forwardData.statusMessage == 'OK') {
		log.info('Successfully updated the task! Status: ' + forwardData.statusMessage);

		if(mainWindow && mainWindow.webContents) mainWindow.webContents.send('add-active-class', { element: 'li[data-id="'+body.ttl_assigned_task_id+'"]' });
	} else { // Retry Here
		log.info('Uh-oh, taskCallback Error! ' + forwardData.statusMessage);

		__this.showPreloader('<p>Updating current task.</p>' + forwardData.statusMessage + '. Retrying after ', retryAfter, true);
		
		setTimeout(function () {
			__this.task(forwardData.task_id, tracker_id, user_id);
		}, retryAfter);
	}
}

exports.timeInTimeOutCallback = (body, forwardData) => {
	__this.hidePreloader();

	var retryAfter = __this.getRetrySecondsRemaining(forwardData.retry_after);

	if(forwardData.time_in) {
		log.info('Saving attendance time-in! Status Code: ' + forwardData.statusMessage);
	} else {
		log.info('Request Name: Time Out! Status Code: ' + forwardData.statusMessage);
	}
	
	if(forwardData.statusMessage == 'OK') {
		log.info('Successfully saved the attendance! Status: ' + forwardData.statusMessage);

		__this.openStats('refresh');
	} else { // Retry Here
		if(forwardData.time_in) {
			__this.showPreloader('<p>Saving attendance time-in</p>' + forwardData.statusMessage + '. Retrying after ', retryAfter, true);
		} else {
			__this.showPreloader('<p>Saving attendance time-out</p>' + forwardData.statusMessage + '. Retrying after ', retryAfter, true);
		}
		
		setTimeout(function () {
			if(forwardData.time_in) {
				__this.timeIn(tracker_id, user_id, forwardData.is_onsite);
			} else {
				__this.timeOut(tracker_id, user_id);
			}
			
		}, retryAfter);
	}
}

exports.loadingCloseCallback = () => {
	__this.hidePreloader();
}

var screenshotRetryCount = 0;
exports.screenShotURLCallback = (body, forwardData) => {
	screenshotReady = true;

	var retryAfter = __this.getRetrySecondsRemaining(forwardData.retry_after);

	if(forwardData.statusMessage == 'OK') {
		log.info('Successfully uploaded the screenshot! Status: ' + forwardData.statusMessage);

		screenshotRetryCount = 0;

		if (screenshotQueueArr.length > 0) {
			var nextQueue = screenshotQueueArr.shift();
			__this.screenShotURL(nextQueue.image, nextQueue.tracker, nextQueue.user);
		}
	} else { // Retry Here
		log.error('Uh-oh, screenShotURLCallback Error! ' + forwardData.statusMessage);
		screenshotRetryCount++;

		if(screenshotRetryCount < 3) {
			setTimeout(function () {
				__this.screenShotURL(forwardData.imageData, tracker_id, user_id);
			}, retryAfter);
		} else {
			screenshotRetryCount = 0;
			screenshotQueueArr.push({ image: forwardData.imageData, tracker: tracker_id, user: user_id });
		}
	}

	if(body && body.data && body.data.newTimeTracker && body.data.newTimeTracker == 1) {
		log.info('Detected a new time tracker record.');

		__this.showPreloader('Retrieving user data automatically');
	 	__this.openStatsCallback(body, forwardData);
	}
}

exports.onlineStatusCallback = (body, forwardData) => {
	log.info('Successfully updated the online status! Status: ' + forwardData.statusMessage);
	if(mainWindow && mainWindow.webContents) mainWindow.webContents.send('online-status-per-minute');
}

var activityRetryCount = 0;
exports.activityTrackerCallback = (body, forwardData) => {
	var retryAfter = __this.getRetrySecondsRemaining(forwardData.retry_after);

	if(forwardData.statusMessage == 'OK') {
		log.info('Successfully updated the activity tracker! Keyboard: ' + forwardData.keyboard_clicks + ' Mouse: ' + forwardData.mouse_clicks + ' Status: ' + forwardData.statusMessage);

		activityRetryCount = 0;

		if (activityQueueArr.length > 0) {
			var nextQueue = activityQueueArr.shift();
			__this.sendUserActivityTracker(nextQueue.tracker_id, nextQueue.user_id, nextQueue.keyboard_clicks, nextQueue.mouse_clicks);
		}
	} else { // Retry Here
		log.error('Uh-oh, activityTrackerCallback Error! ' + forwardData.statusMessage);
		
		activityRetryCount++;

		if(activityRetryCount < 3) {
			setTimeout(function () {
				__this.sendUserActivityTracker(tracker_id, user_id, forwardData.keyboard_clicks, forwardData.mouse_clicks);
			}, retryAfter);
		} else {
			activityRetryCount = 0;

			activityQueueArr.push({ tracker_id: tracker_id, user_id: user_id, keyboard_clicks: forwardData.keyboard_clicks, mouse_clicks: forwardData.mouse_clicks });
		}
	}
}

// End Callbacks
exports.showPreloader = (action, retrySeconds = retryTimeoutDefault, showRetrySeconds = false) => {
	if(mainWindow && mainWindow.webContents) {
		mainWindow.webContents.send('append-action', { action: action, retrySeconds: retrySeconds, showRetrySeconds: showRetrySeconds });
		mainWindow.webContents.send('show-element', { element: '#preloader-background' });
	}
};

exports.hidePreloader = () => {
	if(mainWindow && mainWindow.webContents) {
		mainWindow.webContents.send('remove-action');
		mainWindow.webContents.send('hide-element', { element: '#preloader-background' });
	}
};

exports.getForwardData = () => {
	return {
		'access_token':access_token,
		'host':host,
		'mainWindow':mainWindow,
		'main':__this
	};
}

exports.getRetrySecondsRemaining = (retry_after = null) => {
	var retryTimeout = retryTimeoutDefault; //default to 20 seconds

	if(retry_after > 20) {
		retryTimeout = retry_after * 1000;
	}

	return retryTimeout;
}

exports.openErrorRibbon = (error = null) => {
	log.info('exports.openErrorRibbon starts!');

	isOnline().then((online) => {
		if (online) {
			if(mainWindow && mainWindow.webContents) mainWindow.webContents.send('check-internet', { isOnline: true });
		} else {
			setTimeout(function () {
				__this.openErrorRibbon();
				if(mainWindow && mainWindow.webContents) mainWindow.webContents.send('check-internet', { isOnline: false });
			}, 1000);
		}
	});
};

autoUpdater.on('checking-for-update', () => {
	log.info('Checking for updates...');
});

autoUpdater.on('error', (error) => {
	log.error(`Error in autoUpdater. ${error}`);
	
	if (isSilent) return;
	dialog.showErrorBox(
		'Error during the update',
		`Application couldn't be updated. Please try again or contact the support team.`
	);

	if(mainWindow && mainWindow.webContents) mainWindow.webContents.send('hide-element', { element: '#preloader-background' });
});

autoUpdater.on('update-available', () => {
	log.info('Update available')

	if(mainWindow && mainWindow.webContents) mainWindow.webContents.send('show-element', { element: '#updater-background' });

	autoUpdater.downloadUpdate();
});

autoUpdater.on('download-progress', (progressObj) => {
	let log_message = Math.floor(progressObj.percent) + '%';

	if(mainWindow && mainWindow.webContents) mainWindow.webContents.send('update-bar', { percentage: log_message });
});

autoUpdater.on('update-not-available', () => {
	if(mainWindow && mainWindow.webContents) mainWindow.webContents.send('hide-element', { element: '#preloader-background' });
});

autoUpdater.on('update-downloaded', () => {
	log.info('Update downloaded');

	updateDownloaded = true;
	if (isSilent) return;
	dialog
		.showMessageBox({
			title: 'Install Updates',
			message: 'Updates are ready to be installed.',
			defaultId: 0,
			cancelId: 1,
			buttons: [ 'Install and restart' ]
		})
		.then((buttonIndex) => {
			if (buttonIndex.response === 0) {
				setImmediate(() => autoUpdater.quitAndInstall());
			}
		});
});

exports.checkForUpdates = (silent) => {
	isSilent = silent;
	if (updateDownloaded) {
		dialog
			.showMessageBox({
				title: 'Install Updates',
				message: 'Updates are ready to be installed.',
				defaultId: 0,
				cancelId: 1,
				buttons: [ 'Install and restart' ]
			})
			.then((buttonIndex) => {
				if (buttonIndex.response === 0) {
					setImmediate(() => autoUpdater.quitAndInstall());
				}
			});
	} else {
		autoUpdater.checkForUpdates();
	}
};

if (!gotTheLock) {
	log.info('Closed the app on tray!');
	app.quit();
} else {
	app.on('second-instance', (event, commandLine, workingDirectory) => {
		// Someone tried to run a second instance, we should focus our window.
		if (mainWindow) {
			if (mainWindow.isMinimized()) mainWindow.restore();
			mainWindow.show();
			mainWindow.focus();
		}
	});
}
